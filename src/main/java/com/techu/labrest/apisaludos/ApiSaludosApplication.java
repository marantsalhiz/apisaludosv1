package com.techu.labrest.apisaludos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSaludosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSaludosApplication.class, args);
	}

}
